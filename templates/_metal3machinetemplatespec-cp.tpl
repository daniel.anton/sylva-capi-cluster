{{ define "Metal3MachineTemplateSpec-CP" }}
# https://github.com/metal3-io/cluster-api-provider-metal3/blob/main/examples/machinedeployment/machinedeployment.yaml
dataTemplate:
  name: {{ .Values.name }}-cp-metadata-{{ include "Metal3DataTemplateSpec-CP" . | sha1sum | trunc 10 }}
hostSelector: {{ .Values.control_plane.capm3.hostSelector | toYaml | nindent 2 }}
image:
  checksum: {{ pluck "machine_image_checksum" .Values.capm3 (.Values.control_plane.capm3 | default dict) | last }}
  checksumType: {{ pluck "machine_image_checksum_type" .Values.capm3 (.Values.control_plane.capm3 | default dict) | last }}
  format: {{ pluck "machine_image_format" .Values.capm3 (.Values.control_plane.capm3 | default dict) | last }}
  url: {{ pluck "machine_image_url" .Values.capm3 (.Values.control_plane.capm3 | default dict) | last }}
{{ end }}

{{ define "Metal3DataTemplate-CP" }}
apiVersion: {{ .Values.apiVersions.Metal3DataTemplate }}
kind: Metal3DataTemplate
metadata:
  name: {{ .Values.name }}-cp-metadata-{{ include "Metal3DataTemplateSpec-CP" . | sha1sum | trunc 10 }}
  namespace: {{ .Release.Namespace }}
spec:
# NOTE: the Metal3DataTemplate.spec fields .metaData & .networkData are immutable
# per https://github.com/metal3-io/cluster-api-provider-metal3/blob/main/docs/api.md#updating-metadata-and-networkdata
{{ include "Metal3DataTemplateSpec-CP" . | indent 2 }}
{{ end }}

{{ define "Metal3DataTemplateSpec-CP" }}
# https://gitlab.com/sylva-projects/sylva-core/-/merge_requests/368
# TODO: try to templatize, get diff with https://github.com/metal3-io/cluster-api-provider-metal3/blob/6a9fe1d9e9fbea791094a709d5e79ad99c63b34b/examples/machinedeployment/machinedeployment.yaml#LL56C1-L136C18
clusterName: {{ .Values.name }}
metaData:
  ipAddressesFromIPPool:
  - kind: IPPool
    apiGroup: ipam.metal3.io
    name: {{ printf "%s-%s" .Values.name .Values.capm3.provisioning_pool_name }}
    key: provisioningIP
  objectNames:
  - key: name
    object: machine
  - key: local-hostname
    object: machine
  - key: local_hostname
    object: machine
  prefixesFromIPPool:
  - kind: IPPool
    apiGroup: ipam.metal3.io
    name: {{ printf "%s-%s" .Values.name .Values.capm3.provisioning_pool_name }}
    key: provisioningCIDR
  {{- if .Values.enable_longhorn }}
  fromLabels:
  - key: longhorn
    object: baremetalhost
    label: longhorn
  fromAnnotations:
  - key: disk
    object: baremetalhost
    annotation: disk
  {{- end }}
networkData:
  links:
    ethernets:
    {{- range $interface_name, $interface_def := .Values.control_plane.network_interfaces }}
    {{- if not (hasKey $interface_def "bond_mode") }}
    - id: {{ $interface_name }}
      macAddress:
        fromHostInterface: {{ $interface_name }}
      type: {{ dig "type" "phy" $interface_def  }}
    {{- end }}
    {{- end }}
    bonds:
    {{- range $interface_name, $interface_def := .Values.control_plane.network_interfaces }}
    {{- if hasKey $interface_def "bond_mode" }}
    - id: {{ $interface_name }}
      bondLinks:
        {{- range $bond_interfaces := $interface_def.interfaces }}
        - {{ $bond_interfaces }}
        {{- end }}
      bondMode: {{ $interface_def.bond_mode }}
      macAddress: {}
    {{- end }}
    {{- end }}
    vlans:
    {{- range $interface_name, $interface_def := .Values.control_plane.network_interfaces }}
    {{- if hasKey $interface_def "bond_mode" }}
{{ tuple $ $interface_name $interface_def | include "listOfVlansPerBond" | indent 4 }}
    {{- else }}
{{ tuple $ $interface_name $interface_def | include "listOfVlansPerInterface" | indent 4 }}
    {{- end }}
    {{- end }}
  networks:
    ipv4:
    - id: {{ .Values.control_plane.capm3.provisioning_pool_interface }}
      ipAddressFromIPPool: {{ printf "%s-%s" .Values.name .Values.capm3.provisioning_pool_name }}
      link: {{ .Values.control_plane.capm3.provisioning_pool_interface }}
      routes: {{ pluck "routes" (.Values.capm3.networkData.provisioning_pool_interface | default dict) (.Values.control_plane.capm3.networkData.provisioning_pool_interface | default dict) | last | default list | toYaml | nindent 8 }}
    - id: {{ .Values.control_plane.capm3.public_pool_interface }}
      ipAddressFromIPPool: {{ printf "%s-%s" .Values.name .Values.capm3.public_pool_name }}
      link: {{ .Values.control_plane.capm3.public_pool_interface }}
      {{- if or .Values.capm3.networkData.public_pool_interface .Values.control_plane.capm3.networkData.public_pool_interface }}
      routes:
{{ pluck "routes" .Values.capm3.networkData.public_pool_interface .Values.control_plane.capm3.networkData.public_pool_interface | last | toYaml | indent 8 }}
      {{- else }}
      routes:
        - gateway:
            fromIPPool: {{ printf "%s-%s" .Values.name .Values.capm3.public_pool_name }}
          network: 0.0.0.0
      {{- end }}
  services:
    dns:
    - {{ .Values.capm3.dns_server }}
{{ end }}
