{{- define "RKE2ConfigTemplateSpec" }}
{{- $envAll := index . 0 -}}
{{- $machine_kubelet_extra_args := index . 1 -}}
{{- $machine_rke2_specs := index . 2 -}}
{{- $machine_additional_commands := index . 3 -}}

  {{/*********** Initialize the components of the RKE2ConfigTemplateSpec.spec.template.spec fields */}}
  {{- $base := tuple $envAll $machine_kubelet_extra_args $machine_rke2_specs | include "base-RKE2ConfigTemplateSpec" | fromYaml }}
  {{- $infra := include (printf "%s-RKE2ConfigTemplateSpec" $envAll.Values.capi_providers.infra_provider) $envAll | fromYaml }}

agentConfig: {{ mergeOverwrite $base.agentConfig $infra.agentConfig (dict "kubelet" (dict "extraArgs" (concat $base.agentConfig.kubelet.extraArgs $infra.agentConfig.kubelet.extraArgs))) | toYaml | nindent 2 }}
preRKE2Commands:
  {{ $infra.preRKE2Commands | toYaml | nindent 2 }}
  {{ $base.preRKE2Commands | toYaml | nindent 2 }}
{{- if $machine_additional_commands.pre_bootstrap_commands }}
  {{ $machine_additional_commands.pre_bootstrap_commands | toYaml | nindent 2 }}
{{- end }}
files: {{ concat $base.files $infra.files | toYaml | nindent 2 }}
postRKE2Commands:
{{- if $machine_additional_commands.post_bootstrap_commands }}
  {{ $machine_additional_commands.post_bootstrap_commands | toYaml | nindent 2 }}
{{- else }}
  []
{{- end }}
{{- end }}
