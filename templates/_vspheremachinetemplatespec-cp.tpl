{{ define "VSphereMachineTemplateSpec-CP" }}
cloneMode: {{ .Values.capv.template_clone_mode }}
datacenter: {{ .Values.capv.dataCenter | quote }}
{{- if .Values.capv.dataStore }}
datastore: {{ .Values.capv.dataStore }}
{{- end }}
diskGiB: {{ pluck "diskGiB" .Values.capv (.Values.control_plane.capv | default dict) | last }}
folder: {{ .Values.capv.folder }}
memoryMiB: {{ pluck "memoryMiB" .Values.capv (.Values.control_plane.capv | default dict) | last }}
network:
  devices:
  {{- range $network_key, $network_def := .Values.capv.networks }}
    - dhcp4: {{ $network_def.dhcp4 | default false }}
      networkName: {{ $network_def.networkName }}
  {{- end }}
numCPUs: {{ pluck "numCPUs" .Values.capv (.Values.control_plane.capv | default dict) | last }}
resourcePool: {{ .Values.capv.resourcePool }}
server: {{ .Values.capv.server }}
{{- if .Values.capv.storagePolicyName }}
storagePolicyName: {{ .Values.capv.storagePolicyName }}
{{- end }}
template: {{ pluck "image_name" .Values.capv .Values.control_plane.capv | last }}
thumbprint: {{ .Values.capv.tlsThumbprint }}
{{ end }}
