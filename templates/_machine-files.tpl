
{{/*
Create the registry hosts .toml file
*/}}
{{- define "registry_mirrors" -}}
{{- $registryMirrors := get .Values "registry_mirrors" | default dict -}}
{{- $defaultSettings := get $registryMirrors "default_settings" | default dict -}}
{{- range $registry, $mirrors := get $registryMirrors "hosts_config" | default dict }}
- path: /etc/containerd/registry.d/{{ $registry }}/hosts.toml
  owner: root
  permissions: "0644"
  content: |
    server = "https://{{ $registry }}"
    {{- range $_, $config := $mirrors }}
    [host."{{ $config.mirror_url }}"]
    {{- range $setting,$value := mergeOverwrite (deepCopy $defaultSettings) (get $config "registry_settings" | default dict) }}
      {{ $setting }} = {{ $value | toJson }}
{{- end }}
{{- end }}
{{- end }}
{{ end }}

{{/*
Create the RKE2 containerd config .toml file
*/}}
{{- define "rke2_config_toml" -}}
- path: /var/lib/rancher/rke2/agent/etc/containerd/config.toml.tmpl
  owner: "root:root"
  permissions: "064O"
  encoding: base64
  # Template copied from https://github.com/k3s-io/k3s/blob/master/pkg/agent/templates/templates_linux.go
  # adapted to use /etc/containerd/registry.d directory for registry configuration (to be consistent with kubeadm)
  content: |
{{ .Files.Get "files/rke2.config.toml" | b64enc | indent 4 }}
{{- end }}

{{/*
Create the Kubernetes manifest for kubeadm VIP .yaml file
*/}}
{{- define "kubernetes_kubeadm_vip" -}}
- path: /etc/kubernetes/manifests/kube-vip.yaml
  owner: root:root
  permissions: "0644"
  content: |
    apiVersion: v1
    kind: Pod
    metadata:
      creationTimestamp: null
      name: kube-vip
      namespace: kube-system
    spec:
      containers:
      - args:
        - manager
        env:
        - name: svc_enable
          value: "true"
        - name: vip_arp
          value: "true"
        - name: port
          value: "6443"
        - name: vip_cidr
          value: "32"
        - name: cp_enable
          value: "true"
        - name: cp_namespace
          value: kube-system
        - name: vip_ddns
          value: "false"
        - name: vip_leaderelection
          value: "true"
        - name: vip_leaseduration
          value: "5"
        - name: vip_renewdeadline
          value: "3"
        - name: vip_retryperiod
          value: "1"
        - name: address
          value: {{ .Values.cluster_external_ip }}
        {{- if .Values.bgp_lbs.kube_vip }}
        - name: bgp_enable
          value: "true"
        {{- range $key, $value := .Values.bgp_lbs.kube_vip }}
        - name: {{ $key }}
          value: {{ $value }}
        {{- end }}
        {{- end }}
        - name: prometheus_server
          value: :2112
        # Renovate Bot needs additional information to detect the kube-vip version:
        # renovate: registryUrl=https://ghcr.io image=kube-vip/kube-vip
        image: {{ .Values.images.kube_vip.repository }}:{{ .Values.images.kube_vip.tag }}
        imagePullPolicy: Always
        name: kube-vip
        resources: {}
        securityContext:
          capabilities:
            add:
            - NET_ADMIN
            - NET_RAW
        volumeMounts:
        - mountPath: /etc/kubernetes/admin.conf
          name: kubeconfig
      hostAliases:
      - hostnames:
        - kubernetes
        ip: 127.0.0.1
      hostNetwork: true
      volumes:
      - hostPath:
          path: /etc/kubernetes/admin.conf
        name: kubeconfig
{{- end }}

{{/*
Create the Kubernetes manifest for RKE2 ingress .yaml file
*/}}
{{- define "kubernetes_rke2_ingress" -}}
- path: /var/lib/rancher/rke2/server/manifests/ingress.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
      ---
      apiVersion: helm.cattle.io/v1
      kind: HelmChartConfig
      metadata:
        name: rke2-ingress-nginx
        namespace: kube-system
      spec:
        valuesContent: |-
          controller:
            publishService:
              enabled: true
            hostNetwork: false
            service:
              enabled: true
              loadBalancerIP: "{{ .Values.cluster_external_ip }}"
              annotations:
                metallb.universe.tf/allow-shared-ip: cluster-external-ip
{{- end }}

{{/*
Create the Kubernetes manifest for RKE2 MetalLB .yaml file
*/}}
{{- define "kubernetes_rke2_metallb" -}}
- path: /var/lib/rancher/rke2/server/manifests/metallb.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
      ---
      apiVersion: v1
      kind: Namespace
      metadata:
        name: metallb-system
        labels:
          pod-security.kubernetes.io/enforce: privileged
          pod-security.kubernetes.io/audit: privileged
          pod-security.kubernetes.io/warn: privileged
      ---
      apiVersion: helm.cattle.io/v1
      kind: HelmChart
      metadata:
        name: metallb
        namespace: metallb-system
      spec:
        bootstrap: true
        chart: {{ .Values.metallb_helm_oci_url | default "metallb" }}
        repo: {{ empty .Values.metallb_helm_oci_url | ternary "https://metallb.github.io/metallb" "" }}
        version: {{ .Values.metallb_helm_version }}
        targetNamespace: metallb-system
        valuesContent: |-
          controller:
            nodeSelector:
              node-role.kubernetes.io/control-plane: "true"
            tolerations:
            - key: node.cloudprovider.kubernetes.io/uninitialized
              value: "true"
              effect: NoSchedule
            - effect: NoExecute
              key: node-role.kubernetes.io/etcd
            - effect: NoSchedule
              key: node-role.kubernetes.io/master
            - effect: NoSchedule
              key: node-role.kubernetes.io/control-plane
          speaker:
            nodeSelector:
              node-role.kubernetes.io/control-plane: "true"
            tolerations:
            - key: node.cloudprovider.kubernetes.io/uninitialized
              value: "true"
              effect: NoSchedule
            - effect: NoExecute
              key: node-role.kubernetes.io/etcd
            - effect: NoSchedule
              key: node-role.kubernetes.io/master
            - effect: NoSchedule
              key: node-role.kubernetes.io/control-plane
      ---
      apiVersion: metallb.io/v1beta1
      kind: IPAddressPool
      metadata:
        name: lbpool
        namespace: metallb-system
      spec:
        addresses:
          - {{ .Values.cluster_external_ip }}/32
      ---
      apiVersion: metallb.io/v1beta1
      kind: L2Advertisement
      metadata:
        name: l2advertisement
        namespace: metallb-system
      spec:
        ipAddressPools:
        - lbpool
{{- if eq .Values.capi_providers.infra_provider "capm3" }}
        interfaces: {{ .Values.cluster_external_ip_interfaces | default (list .Values.control_plane.capm3.public_pool_interface) | toYaml | nindent 10 }}
{{- end }}
{{- end }}

{{/*
Create the Kubernetes manifest for RKE2 MetalLB-L3 .yaml file
*/}}
{{- define "kubernetes_rke2_metallb_l3" -}}
{{- if .Values.bgp_lbs.metallb }}
- path: /var/lib/rancher/rke2/server/manifests/metallb-l3.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
    {{- range .Values.bgp_lbs.metallb.address_pools }}
      ---
      apiVersion: metallb.io/v1beta1
      kind: IPAddressPool
      metadata:
        name: {{ .name }}
        namespace: metallb-system
      spec:
        addresses: {{ .addresses | toYaml | nindent 8 }}
    {{- end -}}
    {{- range .Values.bgp_lbs.metallb.l3_options.bgp_peers }}
      ---
      apiVersion: metallb.io/v1beta2
      kind: BGPPeer
      metadata:
        name: {{ .name }}
        namespace: metallb-system
      spec:
        myASN: {{ .local_asn }}
        peerASN: {{ .peer_asn }}
        peerAddress: {{ .peer_address }}
      ---
      apiVersion: metallb.io/v1beta1
      kind: BGPAdvertisement
      metadata:
        name: {{ .name }}
        namespace: metallb-system
      spec:
        ipAddressPools: {{ .advertised_pools | toYaml | nindent 8 }}
        peers:
        - {{ .name }}
    {{- end }}
{{- end }}
{{- end }}

{{/*
Create the Kubernetes manifest for RKE2 VIP .yaml file
*/}}
{{- define "kubernetes_rke2_vip" -}}
- path: /var/lib/rancher/rke2/server/manifests/kubernetes-vip.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
      ---
      apiVersion: v1
      kind: Service
      metadata:
        name: kubernetes-vip
        namespace: kube-system
        annotations:
          metallb.universe.tf/allow-shared-ip: cluster-external-ip
      spec:
        type: LoadBalancer
        loadBalancerIP: {{ .Values.cluster_external_ip }}
        ports:
        - name: https
          port: 6443
          protocol: TCP
          targetPort: 6443
        - name: rancher
          port: 9345
          protocol: TCP
          targetPort: 9345
        selector:
          component: kube-apiserver
{{- end }}

{{/*
Create the Kubernetes manifest for RKE2 CIS profile cert-manager pod security policy .yaml file
*/}}
{{- define "kubernetes_rke2_cert_manager_psp" -}}
- path: /var/lib/rancher/rke2/server/manifests/cert-manager-psp.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
    ---
    apiVersion: policy/v1beta1
    kind: PodSecurityPolicy
    metadata:
      annotations:
        meta.helm.sh/release-name: cert-manager
        meta.helm.sh/release-namespace: cert-manager
        seccomp.security.alpha.kubernetes.io/allowedProfileNames: '*'
      labels:
        app.kubernetes.io/managed-by: Helm
      name: cert-manager
    spec:
      privileged: false
      allowPrivilegeEscalation: false
      allowedCapabilities: []  # default set of capabilities are implicitly allowed
      volumes:
      - 'configMap'
      - 'emptyDir'
      - 'projected'
      - 'secret'
      - 'downwardAPI'
      hostNetwork: false
      hostIPC: false
      hostPID: false
      runAsUser:
        rule: 'MustRunAs'
        ranges:
        - min: 1000
          max: 1000
      seLinux:
        rule: 'RunAsAny'
      supplementalGroups:
        rule: 'MustRunAs'
        ranges:
        - min: 1000
          max: 1000
      fsGroup:
        rule: 'MustRunAs'
        ranges:
        - min: 1000
          max: 1000
{{- end }}

{{/*
Create the Kubernetes manifest for RKE2 CIS profile MetalLB pod security policy, role and role binding .yaml file
*/}}
{{- define "kubernetes_rke2_metallb_psp" -}}
- path: /var/lib/rancher/rke2/server/manifests/metallb-psp.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
    ---
    apiVersion: policy/v1beta1
    kind: PodSecurityPolicy
    metadata:
      labels:
        app.kubernetes.io/instance: metallb
        app.kubernetes.io/name: metallb
      name: metallb-speaker
    spec:
      allowPrivilegeEscalation: false
      allowedCapabilities:
      - NET_ADMIN
      - NET_RAW
      allowedHostPaths: []
      defaultAddCapabilities: []
      defaultAllowPrivilegeEscalation: false
      fsGroup:
        rule: RunAsAny
      hostIPC: false
      hostNetwork: true
      hostPID: false
      hostPorts:
      - max: 7472
        min: 7472
      - max: 7946
        min: 7946
      privileged: true
      readOnlyRootFilesystem: true
      requiredDropCapabilities:
      - ALL
      runAsUser:
        rule: RunAsAny
      seLinux:
        rule: RunAsAny
      supplementalGroups:
        rule: RunAsAny
      volumes:
      - configMap
      - downwardAPI
      - projected
      - secret
      - emptyDir
    ---
    apiVersion: rbac.authorization.k8s.io/v1
    kind: Role
    metadata:
      name: metallb-speaker-role
      namespace: metallb-system
    rules:
    - apiGroups:
      - policy
      resourceNames:
      - metallb-speaker
      resources:
      - podsecuritypolicies
      verbs:
      - use
    ---
    apiVersion: rbac.authorization.k8s.io/v1
    kind: RoleBinding
    metadata:
      name: metallb-speaker-rb
      namespace: metallb-system
    roleRef:
      apiGroup: rbac.authorization.k8s.io
      kind: Role
      name: metallb-speaker-role
    subjects:
    - kind: ServiceAccount
      name: metallb-speaker
{{- end }}

{{/*
Create the Kubernetes manifest for RKE2 CIS profile tigera pod security policy .yaml file
*/}}
{{- define "kubernetes_rke2_tigera_psp" -}}
- path: /var/lib/rancher/rke2/server/manifests/tigera-psp.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
    ---
    apiVersion: policy/v1beta1
    kind: PodSecurityPolicy
    metadata:
      annotations:
        meta.helm.sh/release-name: rke2-calico
        meta.helm.sh/release-namespace: kube-system
        seccomp.security.alpha.kubernetes.io/allowedProfileNames: '*'
      labels:
        app.kubernetes.io/managed-by: Helm
      name: tigera-operator
    spec:
      allowPrivilegeEscalation: false
      fsGroup:
        ranges:
        - max: 65535
          min: 1
        rule: MustRunAs
      hostNetwork: true
      hostPorts:
      - max: 65535
        min: 0
      requiredDropCapabilities:
      - ALL
      runAsUser:
        rule: MustRunAsNonRoot
      seLinux:
        rule: RunAsAny
      supplementalGroups:
        ranges:
        - max: 65535
          min: 1
        rule: MustRunAs
      volumes:
      - hostPath
      - configMap
      - emptyDir
      - projected
      - secret
      - downwardAPI
      - persistentVolumeClaim
{{- end }}

{{/*
Create the resolv.conf file
*/}}
{{- define "resolv_conf" -}}
{{- if eq .Values.capi_providers.infra_provider "capd" }}
- path: /etc/resolv.conf
  owner: root
  permissions: "0644"
  content: |
    nameserver {{ .Values.mgmt_cluster_external_ip }}
{{- else }}
- path: /etc/systemd/resolved.conf
  content: |
    [Resolve]
    DNS={{ .Values.mgmt_cluster_external_ip }}
    Domains=~{{ .Values.mgmt_cluster_external_domain }}
{{- end }}
{{- end }}

{{/*
Create the containerd proxy.conf file
*/}}
{{- define "containerd_proxy_conf" }}
- path: /etc/systemd/system/containerd.service.d/proxy.conf
  owner: root
  permissions: "0644"
  content: |
    [Service]
    Environment="HTTP_PROXY={{ .Values.proxies.http_proxy }}"
    Environment="HTTPS_PROXY={{ .Values.proxies.https_proxy }}"
    Environment="NO_PROXY={{ .Values.proxies.no_proxy }}"
{{- end }}

{{/*
Create the RKE2-server containerd proxy settings file
*/}}
{{- define "rke2_server_containerd_proxy" }}
- path: /etc/default/rke2-server
  owner: root:root
  permissions: "0644"
  content: |
    HTTP_PROXY={{ .Values.proxies.http_proxy }}
    HTTPS_PROXY={{ .Values.proxies.https_proxy }}
    NO_PROXY={{ .Values.proxies.no_proxy }}
{{- end }}

{{/*
Create the RKE2-agent containerd proxy settings file
*/}}
{{- define "rke2_agent_containerd_proxy" }}
- path: /etc/default/rke2-agent
  owner: root:root
  permissions: "0644"
  content: |
    HTTP_PROXY={{ .Values.proxies.http_proxy }}
    HTTPS_PROXY={{ .Values.proxies.https_proxy }}
    NO_PROXY={{ .Values.proxies.no_proxy }}
{{- end }}

{{/*
Configure rke2-calico HelmChart to tolerate node.cloudprovider.kubernetes.io/uninitialized
*/}}
{{- define "rke2_calico_helm_chart_config" }}
- path: /var/lib/rancher/rke2/server/manifests/rke2-calico-toleration.yaml
  owner: root:root
  permissions: "0644"
  content: |
    apiVersion: helm.cattle.io/v1
    kind: HelmChartConfig
    metadata:
      name: rke2-calico
      namespace: kube-system
    spec:
      valuesContent: |-
        installation:
          controlPlaneTolerations:
            - key: "node.cloudprovider.kubernetes.io/uninitialized"
              effect: "NoSchedule"
              value: "true"
{{- end }}

{{/*
Configure rke2-cilium HelmChart to tolerate node.cloudprovider.kubernetes.io/uninitialized
*/}}
{{- define "rke2_cilium_helm_chart_config" }}
- path: /var/lib/rancher/rke2/server/manifests/rke2-cilium-toleration.yaml
  owner: root:root
  permissions: "0644"
  content: |
    apiVersion: helm.cattle.io/v1
    kind: HelmChartConfig
    metadata:
      name: rke2-cilium
      namespace: kube-system
    spec:
      valuesContent: |-
        tolerations:
          - key: "node.cloudprovider.kubernetes.io/uninitialized"
            effect: "NoSchedule"
            value: "true"
            operator: Exists
{{- end }}

{{/*
Configure rke2-coredns HelmChart to tolerate node.cloudprovider.kubernetes.io/uninitialized
*/}}
{{- define "rke2_coredns_helm_chart_config" }}
- path: /var/lib/rancher/rke2/server/manifests/rke2-coredns-toleration.yaml
  owner: root:root
  permissions: "0644"
  content: |
    apiVersion: helm.cattle.io/v1
    kind: HelmChartConfig
    metadata:
      name: rke2-coredns
      namespace: kube-system
    spec:
      valuesContent: |-
        tolerations:
          - key: "node.cloudprovider.kubernetes.io/uninitialized"
            effect: "NoSchedule"
            value: "true"
{{- end }}
