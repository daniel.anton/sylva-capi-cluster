{{- define "DockerMachineTemplateSpec" }}
{{- if and (or .Values.capd.image_name .Values.control_plane.capd.image_name) (eq .Values.capi_providers.bootstrap_provider "cabpr") }}
customImage: {{ pluck "image_name" .Values.capd (.Values.control_plane.capd | default dict) | last }}
{{- end }}
{{- if .Values.capd.docker_host_socket }}
extraMounts:
  - containerPath: {{ .Values.capd.docker_host_socket }}
    hostPath: {{ .Values.capd.docker_host_socket }}
{{- end }}
{{- end }}
