{{- define "XMachineTemplate-CP" }}
---
{{ if eq .Values.capi_providers.infra_provider "capd" }}
apiVersion: {{ .Values.apiVersions.DockerMachineTemplate }}
kind: DockerMachineTemplate
metadata:
  name: {{ .Values.name }}-cp-{{ include "DockerMachineTemplateSpec" . | sha1sum | trunc 10 }}
  namespace: {{ .Release.Namespace }}
spec:
  template:
    spec:
{{ include "DockerMachineTemplateSpec" . | indent 6 }}
{{ else if eq .Values.capi_providers.infra_provider "capo" }}
apiVersion: {{ .Values.apiVersions.OpenStackMachineTemplate }}
kind: OpenStackMachineTemplate
metadata:
  name: {{ .Values.name }}-cp-{{ include "OpenStackMachineTemplateSpec-CP" . | sha1sum | trunc 10 }}
  namespace: {{ .Release.Namespace }}
  labels:
    role: control-plane
spec:
  template:
    spec:
{{ include "OpenStackMachineTemplateSpec-CP" . | indent 6 }}
{{ else if eq .Values.capi_providers.infra_provider "capv" }}
apiVersion: {{ .Values.apiVersions.VSphereMachineTemplate }}
kind: VSphereMachineTemplate
metadata:
  name: {{ .Values.name }}-cp-{{ include "VSphereMachineTemplateSpec-CP" . | sha1sum | trunc 10 }}
  namespace: {{ .Release.Namespace }}
spec:
  template:
    spec:
{{ include "VSphereMachineTemplateSpec-CP" . | indent 6 }}
{{ else if eq .Values.capi_providers.infra_provider "capm3" }}
apiVersion: {{ .Values.apiVersions.Metal3MachineTemplate }}
kind: Metal3MachineTemplate
metadata:
  name: {{ .Values.name }}-cp-{{ include "Metal3MachineTemplateSpec-CP" . | sha1sum | trunc 10 }}
  namespace: {{ .Release.Namespace }}
  labels:
    role: control-plane
spec:
  template:
    spec:
{{ include "Metal3MachineTemplateSpec-CP" . | indent 6 }}
---
{{ include "Metal3DataTemplate-CP" . }}
---
# NOTE: API explained at https://github.com/metal3-io/ip-address-manager/blob/main/docs/api.md#ippool
# TODO: review templating
apiVersion: {{ .Values.apiVersions.IPPool }}
kind: IPPool
metadata:
  name: {{ printf "%s-%s" .Values.name .Values.capm3.provisioning_pool_name }}
  namespace: {{ .Release.Namespace }}
spec:
  clusterName: {{ .Values.name }}
  namePrefix: {{ .Values.name }}-prov
  gateway: {{ .Values.capm3.provisioning_pool_gateway }}
  pools:
  - start: {{ .Values.capm3.provisioning_pool_start }}
    end: {{ .Values.capm3.provisioning_pool_end }}
  prefix: {{ .Values.capm3.provisioning_pool_prefix }}
---
apiVersion: {{ .Values.apiVersions.IPPool }}
kind: IPPool
metadata:
  name: {{ printf "%s-%s" .Values.name .Values.capm3.public_pool_name }}
  namespace: {{ .Release.Namespace }}
spec:
  clusterName: {{ .Values.name }}
  namePrefix: {{ .Values.name }}-bmv4
  gateway: {{ .Values.capm3.public_pool_gateway }}
  pools:
  - start: {{ .Values.capm3.public_pool_start }}
    end: {{ .Values.capm3.public_pool_end }}
  prefix: {{ .Values.capm3.public_pool_prefix }}
{{ end }}
{{- end }}
